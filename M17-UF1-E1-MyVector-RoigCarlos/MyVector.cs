﻿using System;
namespace M17_UF1_E1_MyVector_RoigCarlos
{
	public class MyVector : IComparable<MyVector>, IComparar
	{

		private double[] _MyVector;
		public MyVector(double[] vector)
		{
			_MyVector = vector;
		}
		public double[] GetPinici() { return new[] { _MyVector[0], _MyVector[1] }; }
		public double[] GetPfinal() { return new[] { _MyVector[2], _MyVector[3] }; }
		

		public void SetPinicial(double[] pinici)
		{
			_MyVector[0] = pinici[0];
			_MyVector[1] = pinici[1];
		}


		public void SetPfinal(double[] pfinal)
		{
			_MyVector[2] = pfinal[0];
			_MyVector[3] = pfinal[1];
		}

		public double DistanciaVector()
        {
			return Math.Sqrt(Math.Pow(_MyVector[2] - _MyVector[0], 2) + Math.Pow(_MyVector[3] - _MyVector[1], 2));
        }

		public void ModSentit()
        {
			(_MyVector[0], _MyVector[2]) = (_MyVector[2], _MyVector[0]);
			(_MyVector[1], _MyVector[3]) = (_MyVector[3], _MyVector[1]);
		}

        public override string ToString()
        {
			return "punt inici: (" + _MyVector[0] + " ," + _MyVector[1] + ")   punt final : (" + _MyVector[2] + " ," + _MyVector[3] + ")" + " distancia: " + DistanciaVector();
        }

        public int CompareTo(MyVector vector)
        {
			if (InData.PointCloseTo0(GetPinici(), GetPfinal()) < InData.PointCloseTo0(vector.GetPinici(), vector.GetPfinal()))
				return 1;
			return -1; 
		}

        public bool CompararA(MyVector vector)
        {
			if (DistanciaVector() > vector.DistanciaVector())
				return true;
			return false;
		}
    }
}
