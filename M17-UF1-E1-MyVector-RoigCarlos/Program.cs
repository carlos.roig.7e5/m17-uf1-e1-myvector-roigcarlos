﻿using System;

namespace M17_UF1_E1_MyVector_RoigCarlos
{
    class Program
    {
        public static void Main()
        {
            var program = new Program();
            program.Inici();
        }

        public void Inici()
        {
            do
            {
                MostrarMenu( new[] {"[1]. Funcions Hello {name}","[2]. Funcions MyVector.", "[3]. Funcions VectorGame", "[0]. Sortir"});
            } while (!OpcioMenu());
        }

        public void MostrarMenu(string[] enunciats)
        {
            foreach (var enunciat in enunciats)
                Console.WriteLine(enunciat);
        }

        public bool OpcioMenu()
        {
            switch (InData.InString("Introdueix un número del 0 al 3:"))
            {
                case "0":
                    return true;
                case "1":
                    Helloname name = null;
                    do
                    {
                        MostrarMenu( new [] {"[0]. Sortir", "[1]. Escriure un nom", "[2]. Mostrar missatge"});
                    } while (!OpcioMenuHelloname(ref name));
                    break;
                case "2":
                    MyVector myVector = null;
                    do
                    {
                        MostrarMenu( new [] {"[0]. Sortir", "[1]. Crear Vector", "[2]. Mostrar Valors", "[3]. Cambiar valors", "[4]. Mostrar Distancia Vector", "[5]. Canviar Direcció", "[6]. Mostrar Vector."});
                    } while (!OpcioMenuMyVector(ref myVector));
                    break;
                case "3":
                    MyVector[] vectors = null;
                    do
                    {
                        MostrarMenu( new [] {"[0]. Sortir", "[1]. Crear array de random vectors", "[2]. Ordenar valors per longitud", "[3]. Ordenar valor per proximitat a l'origen", "[4]. Mostrar array de vectors."});
                    } while (!OpcioMenuVectorGame(ref vectors));
                    break;
            }

            return false;
        }

        public bool OpcioMenuHelloname(ref Helloname name)
        {
            switch (InData.InString("Introduiex un número del 0 al 2:"))
            {
                case "0":
                    return true;
                case "1":
                    name = new Helloname(InData.InString("Introdueix un nom: "));
                    break;
                case "2":
                    if (name != null)
                        Console.WriteLine(name);
                    else Console.WriteLine("Has de crear primer un nom.");
                    break;
            }

            return false;
        }

        public bool OpcioMenuMyVector(ref MyVector vector)
        {
            switch (InData.InString("Introduiex un número del 0 al 5:"))
            {
                case "0":
                    return true;
                case "1":
                    vector = CrearVector();
                    break;
                case "2":
                    if (vector != null) 
                        MostrarValors(vector);
                    else Console.WriteLine("Has de crear primer el vector.");
                    break;
                case "3":
                    if (vector != null)
                        SetValors(vector);
                    else Console.WriteLine("Has de crear primer el vector.");
                    break;
                        
                case "4":
                    if (vector != null)
                        Console.WriteLine("Distancia del vector : " + vector.DistanciaVector());
                    else Console.WriteLine("Has de crear primer el vector.");
                    break;
                case "5":
                    if (vector != null)
                    {
                        Console.WriteLine("S'ha canviat el sentit del vector");
                        vector.ModSentit();
                    }
                    else Console.WriteLine("Has de crear primer el vector.");
                    break;
                case "6":
                    if (vector!=null)
                        Console.WriteLine(vector);
                    else Console.WriteLine("Has de crear primer el vector.");
                    break;
            }

            return false;
        }
        
        public bool OpcioMenuVectorGame(ref MyVector[] vectors)
        {
            switch (InData.InString("Introduiex un número del 0 al 4:"))
            {
                case "0":
                    return true;
                case "1":
                    vectors = VectorGame.RandomVectors(InData.InInt("Introdueix la llargada del vector"));
                    break;
                case "2":
                    if (vectors != null)
                        VectorGame.SortVectors(vectors, false);
                    else Console.WriteLine("Genera primer un array de vectors aleatoris");
                    break;
                case "3":
                    if (vectors != null)
                      vectors = VectorGame.SortVectors(vectors, true);
                    else Console.WriteLine("Genera primer un array de vectors aleatoris");
                    break;
                        
                case "4":
                    if (vectors != null)
                        VectorGame.MostrarArray(vectors);
                    else Console.WriteLine("Genera primer un array de vectors aleatoris");
                    break;
            }

            return false;
        }

        public MyVector CrearVector()
        {
            return new MyVector(new[]
            {
                InData.InDouble("Introdueix el valor x del punt inicial"),
                InData.InDouble("Introdueix el valor y del punt inicial"),
                InData.InDouble("Introdueix el valor x del punt final"),
                InData.InDouble("Introdueix el valor y del punt final.")
            });
        }

        public void MostrarValors(MyVector vector)
        {
            Console.WriteLine("El punt inicial te les coordenades: ( " + vector.GetPinici()[0] + " ," +
                              vector.GetPinici()[1] + ") \n El punt final te les coordenades: ( " +
                              vector.GetPinici()[0] + " ," + vector.GetPinici()[1] + ")");
        }

        public void SetValors(MyVector vector)
        {
            vector.SetPinicial(new[] {InData.InDouble("Introdueix un nou punt inicial x."), InData.InDouble("Introdueix un nou punt inicial y")});
            vector.SetPfinal(new[] {InData.InDouble("Introdueix un nou punt final x."), InData.InDouble("Introdueix un nou punt final y")});
        }
        
    }
}
