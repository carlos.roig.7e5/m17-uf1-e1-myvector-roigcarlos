﻿public class Helloname
{
	private string _name;
	public Helloname(string name)
	{
		_name = name;
	}
	public override string ToString()
	{
		return "Hello " + _name;
	}

}