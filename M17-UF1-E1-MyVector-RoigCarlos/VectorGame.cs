﻿using System;
namespace M17_UF1_E1_MyVector_RoigCarlos
{
    public class VectorGame
    {
        public static MyVector[] RandomVectors(int llargada)
        {
            var vectors = new MyVector[llargada];
            var rng = new Random();
            for (var i = 0; i < vectors.Length; i++)
                vectors[i] = new MyVector(new double[] { rng.Next(-100, 101), rng.Next(-100, 101), rng.Next(-100, 101), rng.Next(-100, 101)});
            return vectors;    
         }

        public static MyVector[] SortVectors(MyVector[] vectors, bool tipus)
        {
            for(var i = 0; i < vectors.Length - 1; i++)
            {
                for (var y = i; y < vectors.Length; y++)
                {
                    if (!tipus)
                    {
                        if (vectors[i].CompararA(vectors[y]))
                            (vectors[i], vectors[y]) = (vectors[y], vectors[i]);
                    }
                    else 
                        if (vectors[i].CompareTo(vectors[y]) == 1)
                        (vectors[i], vectors[y]) = (vectors[y], vectors[i]);
                }
            }
            return vectors;
        }

        public static void MostrarArray(MyVector[] vectors)
        {
            for (int i = 0; i < vectors.Length; i++)
                Console.WriteLine(" ["+ (i + 1) + "]. " + vectors[i]);
        }

    }
}
